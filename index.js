// Function Declaration - Can be invoked before or after declaration of function
taDa();
function taDa() {
  console.log("Function Declaration");
}
taDa();

// Function Expression - Can only be invoked after the assigning function expression
// Can also reassing a function expression
const taDaAgain = () => {
  console.log("Function Expression");
};
taDaAgain();

let reassignFnEx = () => {
  console.log("Initial assingment");
};
reassignFnEx();

reassignFnEx = () => {
  console.log("Reassignment");
};
reassignFnEx();

// 3 Types of scope

// 1. Local/Block scope
// 2. Global scope
// 3. Function scope


const obj = {
    key: () => {
       console.log("works")
            },
};

obj.key()